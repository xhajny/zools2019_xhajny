//
// Created by Mikulas Muron on 08/04/2020.
//

#include "Zvire.h"

Zvire::Zvire(string jmeno, int hmotnost) {
    m_jmeno = jmeno;
    m_hmotnost = hmotnost;
}

string Zvire::getJmeno() {
    return m_jmeno;
}

int Zvire::getHmotnost() {
    return m_hmotnost;
}

void Zvire::snez(Zvire* korist){
    // Muzu snist jen zvire mensi nez jsem ja sam. Mrtvoly nezeru. Sam sebe taky ne
    if(m_hmotnost > korist->getHmotnost() && ! korist->jeMrtve() && korist != this){
        // zvysim si svoji hmostnost o hmostnost koristi
        m_hmotnost += korist->getHmotnost();
        // oznacim jako mrtve zvire co jsem prave sezral
        korist->umri();

        // debug vypis
        std::cout << getJmeno() << " sezralo " << korist->getJmeno() << std::endl;
    }
}

void Zvire::umri() {
    m_hmotnost = 0;
}

bool Zvire::jeMrtve() {
    return m_hmotnost <= 0;
}

string Zvire::getZnacka() {
    // M oznacime mrtva zvirata
    if(jeMrtve()){
        return "M";
    }
    // Z oznacime ziva zvirata
    else{
        return  "Z";
    }
}


